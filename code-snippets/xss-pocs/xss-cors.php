<?php
// Specify domains from which requests are allowed
header('Access-Control-Allow-Origin: *');
 
// Specify which request methods are allowed
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
 
// Additional headers which may be sent along with the CORS request
 
header('Access-Control-Allow-Headers: X-Requested-With');
 
// Exit early so the page isn't fully loaded for options requests
if (strtolower($_SERVER['REQUEST_METHOD']) == 'options') {
    exit();
}
?>

<div>
 <img style="display:none" src="x" onerror="alert('xss')" />
</div>